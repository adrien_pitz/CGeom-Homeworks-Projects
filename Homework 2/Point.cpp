#include "Point.h"

#include <iostream>

Point::Point():xCoordinate(0), yCoordinate(0), isempty(true){}

Point::Point(long double xCoordinate, long double yCoordinate){
	this->xCoordinate = xCoordinate;
	this->yCoordinate = yCoordinate;
	this->isempty = false;
}

void Point::showPoint(){
    std::cout.precision(1);
	std::cout << "(" << std::fixed << xCoordinate << "," << std::fixed << yCoordinate << ")" << std::endl;
}

long double Point::getXCoordinate() const { return this->xCoordinate; }
long double Point::getYCoordinate() const { return this->yCoordinate; }
void Point::setXCoordinate(long double xCoordinate){ this->xCoordinate = xCoordinate; isempty = false; }
void Point::setYCoordinate(long double yCoordinate){ this->yCoordinate = yCoordinate; isempty = false; }

/**
Returns : 
- -1 if smaller
- 0 if equal
- 1 if greater
**/
int Point::compare(const Point& p1, const Point& p2){
	if(p1.getXCoordinate() < p2.getXCoordinate())
		return -1;
	else{
		if(p1.getXCoordinate() == p2.getXCoordinate()){
		   if(p1.getYCoordinate() > p2.getYCoordinate())  //Ici, un point avec une ordonnée + grande sera avant ceux d'ordonnées + petites dans l'ordre des évènements
			   return 1;
		   else{
		   	   if(p1.getYCoordinate() < p2.getYCoordinate())
			     return -1;
			   else
			   	 return 0;
		   }
	    }
	    return 1;
	}
}

bool Point::isEmpty() const{
	return isempty;
}
    
