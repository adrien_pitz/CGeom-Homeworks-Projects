#include <limits>
#include "Statut.h"
#include <cmath>
#include <iostream>

Statut::Statut():statut(){}

/**
 * Ici il nous faut le segment set pour le test de l'intersection entre les segments, on ne peut pas se contenter de l'ID
 * @param segmentsSet Le set de segments
 * @return /
 */
Statut::Statut(const std::vector<Segment> &segmentsSet) {
    segSet = segmentsSet;
}

/**
 * Add segment ajoute segID au statut
 * @param segId L'id du segment que l'on ajoute à notre statut
 * @return Le résultat du test entre segID et son voisin du dessus et du dessous dans le statut (renvoie un vecteur vide
 * si il n'y a pas de voisins ou tout simplement si il n'y a pas d'intersection)
 */
//std::vector<Point> Statut::addSegment(unsigned int segId, long double y) {

void Statut::addSegment(unsigned int segId, long double y) {

    std::multimap<long double, unsigned int, classcomp>::iterator it = statut.insert(std::pair<long double, unsigned int>(y, segId));
}
    //On check pour d'éventuelles intersections
//    std::vector<Point> intersections;

/*    std::multimap<long double, unsigned int>::iterator upNeighbour, downNeighbour;
    upNeighbour = (--it);
    ++it;
    downNeighbour = (++it);
    --it;

    if(upNeighbour == statut.end() || downNeighbour == statut.end())
        return intersections;

    Point intersect1 = findIntersection(segId, (*upNeighbour).second);
    if(!intersect1.isEmpty())
        intersections.push_back(intersect1);

    Point intersect2 = findIntersection(segId, (*downNeighbour).second);
    if(!intersect2.isEmpty())
        intersections.push_back(intersect2);

    return intersections;*/


std::multimap<long double, unsigned int, classcomp> Statut::getStatut() const{ return statut; }

/**
 *
 * @param segID
 * @return Un point vide si pas d'intersection, un point rempli sinon.
 */
//Point Statut::removeSegment(unsigned int segID) {
void Statut::removeSegment(unsigned int segID) {
    std::multimap<long double, unsigned int, classcomp>::iterator it;
    for(it = statut.begin(); it!= statut.end() ;it++){
        if((*it).second == segID)
            break;
    }

    //Si le segment n'a pas été trouvé, on renvoie un ensemble de points vide
    if(it != statut.end())
//    if(it == statut.end())
//        return Point();
	    statut.erase(it); //On efface le segment}
    //Sinon on retire le segment du statut
    /*std::multimap<long double, unsigned int>::iterator upNeighbour, downNeighbour;
    upNeighbour = (--it);
    ++it;
    downNeighbour = (++it);
    --it;

    statut.erase(it); //On efface le segment*/

    //On fait les tests d'intersection sur base du upper et down neighbor qui sont maintenant adjacents
//    if(upNeighbour == statut.end() || downNeighbour == statut.end())
//        return Point();

//    return findIntersection((*upNeighbour).second, (*downNeighbour).second);
}

std::pair<long double, bool> Statut::findFromSegID(unsigned u) const{
	std::multimap<long double, unsigned int, classcomp>::const_iterator it;
	for(it = statut.begin(); it != statut.end(); ++it){
		if(u == (*it).second){
			return std::make_pair ((*it).first, true );
		}
	}
	return std::make_pair (0, false);
}
void Statut::clear() {
    statut.clear();
}

int Statut::upperNeighbour(unsigned sID) const{
	std::multimap<long double, unsigned int, classcomp>::const_iterator it = statut.begin();
	std::multimap<long double, unsigned int, classcomp>::const_iterator it2 = statut.end();

	for(; it != statut.end(); ++it){
		if((*it).second == sID)
			break;
		it2 = it;
	}
	if(it2 != statut.end())
		return (*it2).second;
	else
		return -1;
}

int Statut::lowerNeighbour(unsigned sID) const{
	std::multimap<long double, unsigned int, classcomp>::const_iterator it = statut.begin();
	std::multimap<long double, unsigned int, classcomp>::const_iterator it2 = statut.end();

	for(; it != statut.end(); ++it){
		if((*it).second == sID)
			break;
	}
	if(it != statut.end())
		++it;
		if(it != statut.end())
			return (*it).second;
	else
		return -1;
}

//returns <lower neighbor, upper neighbor>
// replace a neighbor in the pair by -1 if there is no such neighbor
std::pair<int, int> Statut::getNeighbours(const Point& p) const{
	long double y = p.getYCoordinate();
	std::multimap<long double, unsigned int, classcomp>::const_iterator it = statut.begin();
	std::multimap<long double, unsigned int, classcomp>::const_iterator it2 = statut.end();

	
	for(; it != statut.end(); ++it){
		if((*(it)).first < y){
			//it est donc le voisin du dessous
			//et it2 le voisin du dessus
			if(it2 != statut.end())
				return std::make_pair((*it).second, (*it2).second);
			else
				return std::make_pair((*it).second, -1);
		}
		it2 = it;
	}
	if(it2 != statut.end())
		return std::make_pair(-1, (*it2).second);
		
	return std::make_pair (-1, -1);
}

void Statut::show() {
    std::multimap<long double, unsigned int, classcomp>::const_iterator it;
    std::cout << "[***** STATUT *****]" << std::endl;
    for(it = statut.begin(); it != statut.end() ;++it)
        std::cout << "Seg id : " << (*it).second << "| y : " << (*it).first << std::endl;
    std::cout << "[*****  END   *****]" << std::endl;
}
