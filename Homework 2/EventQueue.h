#ifndef EVENTQUEUE_H
#define EVENTQUEUE_H

#include "Event.h"

#include <list>


class EventQueue
{
public:
	EventQueue();
	void addEvent(const Event&);
	void showQueue();
	bool isEmpty();
	Event getNextEvent();
	bool isInQueue(const Event&) const;
private:
    std::list<Event> queue;

};

#endif
