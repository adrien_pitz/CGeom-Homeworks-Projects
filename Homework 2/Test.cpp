#include "Point.h"
#include "EventQueue.h"
#include "Segment.h"

#include <iostream>
#include <vector>

std::vector<Segment> readIn(std::istream& in){

   std::string line;
   std::vector<int> data;
	std::getline(in, line);
	
   while(std::getline(in, line)){
      std::istringstream iss(line);
      double xCoordOrigin;
      double yCoordOrigin;
      double xCoordExtrem;
      double yCoordExtrem;

	  iss >>
      while(iss >> value)
         data.push_back(value);
   }

   return data;
}

int main(int argc, char *argv[]){


   std::vector<Segment> set;
   
   set.push_back(Segment(Point(0.9,2.2), Point(2.1,1.9)));
   set.push_back(Segment(Point(3.5,4.1), Point(3.9,1.1)));
   set.push_back(Segment(Point(4.2,3.7), Point(5.8,1.8)));
   set.push_back(Segment(Point(5.1,1.2), Point(6.2,3.1)));

   std::cout << "----- Segments: -----" << std::endl;

   std::vector<Segment>::iterator it;
   int i=1;
   for(it = set.begin(); it != set.end() ;++it){
   	std::cout << "Segment n°" << i << std::endl;
   	(*it).getOrigin().showPoint();
   	(*it).getExtremity().showPoint();
   	++i;
   }
   return 0;
}
