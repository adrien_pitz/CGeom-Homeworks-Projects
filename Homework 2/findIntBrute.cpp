#include "Point.h"
#include "Segment.h"

#include <fstream>
#include <limits>
#include <cmath>
#include <sstream>
#include <string>
#include <iostream>

Point findIntersection(const Segment& seg1, const Segment& seg2){

    //Si les deux segments n'ont pas d'abscisse commune, on envoie une erreur
    if(std::max(seg1.getOrigin().getXCoordinate(), seg1.getExtremity().getXCoordinate()) <
            std::min(seg2.getOrigin().getXCoordinate(), seg2.getExtremity().getXCoordinate()))
        return Point();

    //L'équation d'une droite passant par un segment : y = ax + b
    //On va calculer a et b
    bool isSeg1Vertical = false;
    bool isSeg2Vertical = false;
    if(fabsl(seg1.getOrigin().getXCoordinate() - seg1.getExtremity().getXCoordinate()) < std::numeric_limits<long double>::epsilon()*pow(10.0,10.0))
        isSeg1Vertical = true;
    if(fabsl(seg2.getOrigin().getXCoordinate() - seg2.getExtremity().getXCoordinate()) < std::numeric_limits<long double>::epsilon()*pow(10.0,10.0))
        isSeg2Vertical = true;

    if(!isSeg1Vertical && !isSeg2Vertical) {
        long double seg1A = (seg1.getExtremity().getYCoordinate() - seg1.getOrigin().getYCoordinate()) /
                            (seg1.getExtremity().getXCoordinate() - seg1.getOrigin().getXCoordinate());
        long double seg2A = (seg2.getExtremity().getYCoordinate() - seg2.getOrigin().getYCoordinate()) /
                            (seg2.getExtremity().getXCoordinate() - seg2.getOrigin().getXCoordinate());
        long double seg1B = seg1.getOrigin().getYCoordinate() - seg1A * seg1.getOrigin().getXCoordinate();
        long double seg2B = seg2.getOrigin().getYCoordinate() - seg2A * seg2.getOrigin().getXCoordinate();

        //Si les deux segments ont la même pente (ils sont //), alors ils n'ont pas d'intersection !
        if (fabsl(seg1A - seg2A) < std::numeric_limits<long double>::epsilon()*pow(10.0,10.0))
            return Point();

        //Les deux segments ne sont pas parallèles, du coup ils possèdent une intersection !
        long double interX, interY;
        interX = (seg2B - seg1B) / (seg1A - seg2A);

        //On vérifie que l'abscisse est dans les bonne bounds
        if ((interX < std::max(std::min(seg1.getOrigin().getXCoordinate(), seg1.getExtremity().getXCoordinate()),
                               std::min(seg2.getOrigin().getXCoordinate(), seg2.getExtremity().getXCoordinate()))) ||
            (interX > std::min(std::max(seg1.getOrigin().getXCoordinate(), seg1.getExtremity().getXCoordinate()),
                               std::max(seg2.getOrigin().getXCoordinate(), seg2.getExtremity().getXCoordinate()))))
            return Point();

        //Si c'est OK alors on calcule l'ordonnée
        interY = seg1A * interX + seg1B;

        //On regarde alors si l'ordonnée est dans les bounds
        if ((interY < std::max(std::min(seg1.getOrigin().getYCoordinate(), seg1.getExtremity().getYCoordinate()),
                               std::min(seg2.getOrigin().getYCoordinate(), seg2.getExtremity().getYCoordinate()))) ||
            (interY > std::min(std::max(seg1.getOrigin().getYCoordinate(), seg1.getExtremity().getYCoordinate()),
                               std::max(seg2.getOrigin().getYCoordinate(), seg2.getExtremity().getYCoordinate()))))
            return Point();

        //Si tout va bien, on renvoie l'intersection

        return Point(interX, interY);

    }else{
        if(isSeg1Vertical && isSeg2Vertical){
                return Point();
        }else{
            if(isSeg1Vertical){
                long double seg2A = (seg2.getExtremity().getYCoordinate() - seg2.getOrigin().getYCoordinate()) /
                                    (seg2.getExtremity().getXCoordinate() - seg2.getOrigin().getXCoordinate());
                long double seg2B = seg2.getOrigin().getYCoordinate() - seg2A * seg2.getOrigin().getXCoordinate();

                long double y = seg2A * seg1.getOrigin().getXCoordinate() + seg2B;
                if(y < std::min(seg1.getOrigin().getYCoordinate(), seg1.getExtremity().getYCoordinate()) ||
                        y > std::max(seg1.getOrigin().getYCoordinate(), seg1.getExtremity().getYCoordinate()))
                    return Point();

                return Point(seg1.getOrigin().getXCoordinate(), y);
            }else{ //isSeg2Vertical
                long double seg1A = (seg1.getExtremity().getYCoordinate() - seg1.getOrigin().getYCoordinate()) /
                                    (seg1.getExtremity().getXCoordinate() - seg1.getOrigin().getXCoordinate());
                long double seg1B = seg1.getOrigin().getYCoordinate() - seg1A * seg1.getOrigin().getXCoordinate();

                long double y = seg1A * seg2.getOrigin().getXCoordinate() + seg1B;
                if(y < std::min(seg2.getOrigin().getYCoordinate(), seg2.getExtremity().getYCoordinate()) ||
                   y > std::max(seg2.getOrigin().getYCoordinate(), seg2.getExtremity().getYCoordinate()))
                    return Point();

                return Point(seg2.getOrigin().getXCoordinate(), y);
            }
        }
    }
}

std::vector<Segment> readIn(std::istream& in){

    std::string line;
    std::vector<Segment> segments;
    while(std::getline(in, line)){
        std::istringstream iss(line);
        long double x = 0.0, y = 0.0;
        iss >> x;
        iss >> y;
        Point p (x, y);
        iss >> x;
        iss >> y;
        Point p2 (x, y);
        segments.push_back(Segment (p, p2));
    }

    return segments;
}

int main(int argc, char* argv[]){
	std::ifstream ifs ("test.txt");
	std::vector<Segment> segmts = readIn(ifs);
	std::vector<Point> intersections;

	unsigned i = 0, j = 0;
	for(; i < segmts.size(); ++i){
		Segment seg = segmts[i];
		for(j = i + 1; j < segmts.size(); ++j)
			intersections.push_back(findIntersection(seg, segmts[j]));
	}
	
	std::cout << "----- intersections: -----" << std::endl;
    for(j = 0; j < intersections.size(); ++j)
        intersections[j].showPoint();
        
	return 0;
}
