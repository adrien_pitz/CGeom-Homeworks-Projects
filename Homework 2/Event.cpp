#include "Event.h"

Event::Event(const Point& pt){
	this->pt = pt;
}

/**
    ///!\\\ ON UTILISE CETTE FONCTION QUE SI ON AJOUTE UNE EXTREMITE GAUCHE
*/
void Event::addLSegID(unsigned segmt){
	this->leftsegmts.push_back(segmt);
}

std::vector<unsigned> Event::getLSegIDs() const{
	return leftsegmts;
}

Point Event::getPoint() const{
	return pt;
}

bool Event::compare(const Event& evt1, const Event& evt2) {
    Point evt1Pt = evt1.getPoint();
	Point evt2Pt = evt2.getPoint();
	return evt1Pt.getXCoordinate() == evt2Pt.getXCoordinate() && evt1Pt.getYCoordinate() == evt2Pt.getYCoordinate();
}


