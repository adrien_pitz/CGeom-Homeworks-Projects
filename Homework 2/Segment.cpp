#include "Segment.h"
#include <cmath>
#include <limits>
#include <iostream>

/*In case of a sweeping from left to right the origin is the left extremity,
 *while extremity is the right one. Of course it is the contrary for
 *a right to left sweeping. Also, if a segment is vertical
 *(i.e his x-axis coordinates are the same for both extremities) then the origin
 *is the upper extremity.
 *
*/

Segment::Segment():origin(), extremity(){}

Segment::Segment(const Point& origin, const Point& extremity){

	this->origin = origin;
	this->extremity = extremity;

}

Point Segment::getOrigin() const{
	return origin;
}

Point Segment::getExtremity() const{
    return extremity;
}

long double Segment::updateY(const Point& pt, long double prevY){
    long double segXLeft, segYLeft, segXRight, segYRight;
    segXLeft = origin.getXCoordinate();
    segYLeft = origin.getYCoordinate();
    segXRight = extremity.getXCoordinate();
    segYRight = extremity.getYCoordinate();
    
    //We are only interested to segments that are to the left of the 
    //the point we are considering as the status contains only such segments.
    
    if(Point::compare(pt, origin) == 0 || 
        (segXRight - segXLeft) < std::numeric_limits<long double>::epsilon()*pow(10.0,10.0))
        return prevY;
        
    //Equation de la droite : y = slope*x + b
    long double slope = (segYRight - segYLeft) / (segXRight - segXLeft);
    long double b = segYLeft - slope*segXLeft;
    
    long double y = pt.getXCoordinate() * slope + b;
    
    return y - prevY;
}

/**
 *
 * @return
 * 0 if toTest does not belong to the segment
 * 1 if toTest is the 'right point'
 * 2 if toTest is the 'left point'
 * 3 if to Test is strictly inside our segment
 */
int Segment::belongingTest(const Point & toTest) const {
	if(Point::compare(toTest, origin) == 0){
		return 2;
	}else{
		if(Point::compare(toTest, extremity) == 0){
            return 1;
		}else{

            long double segXLeft, segYLeft, segXRight, segYRight;
            segXLeft = origin.getXCoordinate();
            segYLeft = origin.getYCoordinate();
            segXRight =extremity.getXCoordinate();
            segYRight = extremity.getYCoordinate();

            //Si on a une pente = à 0, on check si le segment est bien sur le segment qui est vertical du coup
            if(fabsl(segXRight - segXLeft) < std::numeric_limits<long double>::epsilon()*pow(10.0,10.0)){
                //On check si le point est dans le segment
                //if(toTest.getXCoordinate() >= segXLeft && toTest.getXCoordinate() <= segXRight){
                    if(segYLeft < segYRight){
                        if(toTest.getYCoordinate() >= segYLeft && toTest.getYCoordinate() <= segYRight)
                            return 3;
                        else
                            return 0;
                    }else{
                        if(toTest.getYCoordinate() <= segYLeft && toTest.getYCoordinate() >= segYRight)
                            return 3;
                        else
                            return 0;
                    }
                //}

                //Si le check n'a pas été concluant
                //return 0;
            }


            //Equation de la droite : y = slope*x + b
            long double slope = (segYRight - segYLeft) / (segXRight - segXLeft);
            long double b = segYLeft - slope*segXLeft;
            std::cout.precision(10);
            std::cout << "toTestcoord : " << std::fixed << toTest.getYCoordinate() << " | other : " << slope*toTest.getXCoordinate() + b << std::endl;

            long double res = toTest.getYCoordinate() - (slope*toTest.getXCoordinate() + b);

            //Si ça appartient
            if(fabsl(res) < std::numeric_limits<long double>::epsilon()*pow(10.0,10.0))
                return 3;



            //Si ça n'appartient pas
            return 0;
        }
	}
}

