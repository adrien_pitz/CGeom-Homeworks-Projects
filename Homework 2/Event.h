#ifndef EVENT_H
#define EVENT_H

#include "Point.h"
#include <vector>

class Event{

public:
	Event(const Point&);
    void addLSegID(unsigned);
    std::vector<unsigned> getLSegIDs() const;
    Point getPoint() const;
    static bool compare(const Event &, const Event &);
private:
	Point pt;
	std::vector<unsigned> leftsegmts; //segments for which event(point) is left
									  //extremity

};

#endif
