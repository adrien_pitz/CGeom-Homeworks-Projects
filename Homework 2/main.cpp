#include "Point.h"
#include "EventQueue.h"
#include "Segment.h"
#include "Statut.h"

#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <utility>
#include <cmath>
#include <limits>

/**
 *
 * @param segID1
 * @param segID2
 * @return Un point vide si pas d'intersection ou erreur
 *         Un point valide sinon, qui est le point d'intersection
 */
Point findIntersection(const Segment& seg1, const Segment& seg2){

    //Si les deux segments n'ont pas d'abscisse commune, on envoie une erreur
    if(std::max(seg1.getOrigin().getXCoordinate(), seg1.getExtremity().getXCoordinate()) <
            std::min(seg2.getOrigin().getXCoordinate(), seg2.getExtremity().getXCoordinate()))
        return Point();

    //L'équation d'une droite passant par un segment : y = ax + b
    //On va calculer a et b
    bool isSeg1Vertical = false;
    bool isSeg2Vertical = false;
    if(fabsl(seg1.getOrigin().getXCoordinate() - seg1.getExtremity().getXCoordinate()) < std::numeric_limits<long double>::epsilon()*pow(10.0,10.0))
        isSeg1Vertical = true;
    if(fabsl(seg2.getOrigin().getXCoordinate() - seg2.getExtremity().getXCoordinate()) < std::numeric_limits<long double>::epsilon()*pow(10.0,10.0))
        isSeg2Vertical = true;

    if(!isSeg1Vertical && !isSeg2Vertical) {
        long double seg1A = (seg1.getExtremity().getYCoordinate() - seg1.getOrigin().getYCoordinate()) /
                            (seg1.getExtremity().getXCoordinate() - seg1.getOrigin().getXCoordinate());
        long double seg2A = (seg2.getExtremity().getYCoordinate() - seg2.getOrigin().getYCoordinate()) /
                            (seg2.getExtremity().getXCoordinate() - seg2.getOrigin().getXCoordinate());
        long double seg1B = seg1.getOrigin().getYCoordinate() - seg1A * seg1.getOrigin().getXCoordinate();
        long double seg2B = seg2.getOrigin().getYCoordinate() - seg2A * seg2.getOrigin().getXCoordinate();

        //Si les deux segments ont la même pente (ils sont //), alors ils n'ont pas d'intersection !
        if (fabsl(seg1A - seg2A) < std::numeric_limits<long double>::epsilon()*pow(10.0,10.0))
            return Point();

        //Les deux segments ne sont pas parallèles, du coup ils possèdent une intersection !
        long double interX, interY;
        interX = (seg2B - seg1B) / (seg1A - seg2A);

        //On vérifie que l'abscisse est dans les bonne bounds
        if ((interX < std::max(std::min(seg1.getOrigin().getXCoordinate(), seg1.getExtremity().getXCoordinate()),
                               std::min(seg2.getOrigin().getXCoordinate(), seg2.getExtremity().getXCoordinate()))) ||
            (interX > std::min(std::max(seg1.getOrigin().getXCoordinate(), seg1.getExtremity().getXCoordinate()),
                               std::max(seg2.getOrigin().getXCoordinate(), seg2.getExtremity().getXCoordinate()))))
            return Point();

        //Si c'est OK alors on calcule l'ordonnée
        interY = seg1A * interX + seg1B;

        //On regarde alors si l'ordonnée est dans les bounds
        if ((interY < std::max(std::min(seg1.getOrigin().getYCoordinate(), seg1.getExtremity().getYCoordinate()),
                               std::min(seg2.getOrigin().getYCoordinate(), seg2.getExtremity().getYCoordinate()))) ||
            (interY > std::min(std::max(seg1.getOrigin().getYCoordinate(), seg1.getExtremity().getYCoordinate()),
                               std::max(seg2.getOrigin().getYCoordinate(), seg2.getExtremity().getYCoordinate()))))
            return Point();

        //Si tout va bien, on renvoie l'intersection

        return Point(interX, interY);

    }else{
        if(isSeg1Vertical && isSeg2Vertical){
                return Point();
        }else{
            if(isSeg1Vertical){
                long double seg2A = (seg2.getExtremity().getYCoordinate() - seg2.getOrigin().getYCoordinate()) /
                                    (seg2.getExtremity().getXCoordinate() - seg2.getOrigin().getXCoordinate());
                long double seg2B = seg2.getOrigin().getYCoordinate() - seg2A * seg2.getOrigin().getXCoordinate();

                long double y = seg2A * seg1.getOrigin().getXCoordinate() + seg2B;
                if(y < std::min(seg1.getOrigin().getYCoordinate(), seg1.getExtremity().getYCoordinate()) ||
                        y > std::max(seg1.getOrigin().getYCoordinate(), seg1.getExtremity().getYCoordinate()))
                    return Point();

                return Point(seg1.getOrigin().getXCoordinate(), y);
            }else{ //isSeg2Vertical
                long double seg1A = (seg1.getExtremity().getYCoordinate() - seg1.getOrigin().getYCoordinate()) /
                                    (seg1.getExtremity().getXCoordinate() - seg1.getOrigin().getXCoordinate());
                long double seg1B = seg1.getOrigin().getYCoordinate() - seg1A * seg1.getOrigin().getXCoordinate();

                long double y = seg1A * seg2.getOrigin().getXCoordinate() + seg1B;
                if(y < std::min(seg2.getOrigin().getYCoordinate(), seg2.getExtremity().getYCoordinate()) ||
                   y > std::max(seg2.getOrigin().getYCoordinate(), seg2.getExtremity().getYCoordinate()))
                    return Point();

                return Point(seg2.getOrigin().getXCoordinate(), y);
            }
        }
    }
}


std::vector<unsigned> set_union(const std::vector<std::vector<unsigned> >& s){
    std::vector<std::vector<unsigned> >::const_iterator it = s.begin();
    std::set<unsigned> sunion;

    for(; it != s.end(); ++it)
        sunion.insert((*it).begin(), (*it).end());

    std::vector<unsigned> result(sunion.begin(), sunion.end());
    return result;
}

void trouveEvenement(const Segment& s1 , const Segment& s2, const Point& p, EventQueue& F){
    Point pint = findIntersection(s1, s2);
    if(!pint.isEmpty()){
        if(((pint.getYCoordinate() > p.getYCoordinate() && pint.getXCoordinate() == p.getXCoordinate()) || 
            pint.getXCoordinate() > p.getXCoordinate()) && !F.isInQueue(pint))
                F.addEvent(Event (pint));
    }
}
std::vector<Point> traiteEvenement(const Event& evt, Statut& T, 
                                    const std::vector<Segment> set, EventQueue& F){
    std::vector<Point> I;
    std::vector<unsigned> L = evt.getLSegIDs();
    std::vector<unsigned> R;
    std::vector<unsigned> C;
    std::vector<unsigned>::iterator it;

    std::multimap<long double, unsigned int, classcomp> statut = T.getStatut();
    std::multimap<long double, unsigned int, classcomp>::iterator mapit = statut.begin();
    
    // Here, we add each segments of T to either C, R or none of them.
    for(; mapit != statut.end(); ++mapit){
        unsigned segID = (*mapit).second;
        int test = set[segID].belongingTest(evt.getPoint());
        if(test == 1)
            R.push_back(segID);
        else
            if(test == 3)
                C.push_back(segID);
    }
    
    std::vector<std::vector<unsigned> > LC;
    LC.push_back(L);
    LC.push_back(C);
    
    std::vector<std::vector<unsigned> > RC;
    LC.push_back(R);
    LC.push_back(C);
    
    std::vector<std::vector<unsigned> > LRC (LC.begin(), LC.end());
    LRC.push_back(R);
    
    std::vector<std::vector<unsigned> >::iterator itunion = LRC.begin(); 

    std::vector<unsigned> sunion = set_union(LRC);
    std::vector<unsigned> lcunion = set_union(LC);
    std::vector<unsigned> rcunion = set_union(RC);
    
    if(sunion.size() > 1)
        //P est une intersection
        I.push_back(evt.getPoint());
        
    for(it = rcunion.begin(); it != rcunion.end(); ++it)
        T.removeSegment((*it));

    //We need to modify the y coordinates of the elements of the statut
    for(it = lcunion.begin(); it != lcunion.end(); ++it){
        Segment seg = set[(*it)];
        std::pair<long double, bool> findPrevY = T.findFromSegID((*it));
        T.removeSegment((*it));
        if(findPrevY.second)
            T.addSegment((*it), findPrevY.first + seg.updateY(evt.getPoint(), findPrevY.first));
        else
            T.addSegment((*it), (seg.getOrigin().getYCoordinate()));
    }
    if(lcunion.size() == 0){
        //TODO Check if they exist 
        std::pair<int, int> neighbors = T.getNeighbours(evt.getPoint());
        Segment su = set[neighbors.second], sd = set[neighbors.first];
        trouveEvenement(su, sd, evt.getPoint(), F);
    }
    else{
        //TODO Check if they exist
        unsigned s = lcunion[0];
        int su = T.upperNeighbour(s);
        if(su != -1)
            trouveEvenement(set[su], set[s], evt.getPoint(), F);
        s = lcunion[lcunion.size() - 1];
        su = T.lowerNeighbour(s);
        if(su != -1)
            trouveEvenement(set[su], set[s], evt.getPoint(), F);
            
    }
    return I;
}

std::vector<Point> trouveIntersection(const std::vector<Segment>& segmentsSet){
     
    //On initialise le vecteur qui va contenir la solution
    std::vector<Point> intersectionPoints;

    //On initialise une file d'évènement vide
    EventQueue eventsQueue;
    Statut T (segmentsSet);
    unsigned segid = 0;

    //On insère les points extrémités des segments de segmentsSet dans la file.
    //Si le point est un point situé à gauche du segment, on y attache le segment
    std::vector<Segment>::const_iterator segmtSetIt = segmentsSet.begin();
    for (; segmtSetIt!=segmentsSet.end(); ++segmtSetIt)
    {
        const Segment seg = (*segmtSetIt);
        //On ajoute l'origine du segment (aka le point à gauche)
        Event evtOrigin (seg.getOrigin());
        evtOrigin.addLSegID(segid);  //ON OUBLIE PAS D'ATTACHER LE SEGMENT
        eventsQueue.addEvent(evtOrigin);
        //On ajoute l'extrémité du segment (aka le point à droite)
        Event evtExtremity (seg.getExtremity());
        eventsQueue.addEvent(evtExtremity);
        ++segid;
    }

    eventsQueue.showQueue();

    while (! eventsQueue.isEmpty())
    {
        Event evtToHandle = eventsQueue.getNextEvent();
        std::vector<Point> intersections = traiteEvenement(evtToHandle, T, segmentsSet, eventsQueue);
        for(unsigned i = 0; i < intersections.size(); ++i)
            intersectionPoints.push_back(intersections[i]);
    }

    return intersectionPoints;

}

std::vector<Segment> readIn(std::istream& in){

    std::string line;
    std::vector<Segment> segments;
    while(std::getline(in, line)){
        std::istringstream iss(line);
        long double x = 0.0, y = 0.0;
        iss >> x;
        iss >> y;
        Point p (x, y);
        iss >> x;
        iss >> y;
        Point p2 (x, y);
        segments.push_back(Segment (p, p2));
    }

    return segments;
}


int main(int argc, char *argv[]){

    if(argc < 2) {
        std::cout << "Error usage is : program filename" << std::endl;
        return -1;
    }
    
    std::ifstream ifs (argv[1]);
    const std::vector<Segment> set = readIn(ifs);
	std::cout << "----- Segments: -----" << std::endl;

   std::vector<Segment>::const_iterator it;
   int i=1;
   for(it = set.begin(); it != set.end() ;++it){
       std::cout << "Segment n°" << i << std::endl;
       (*it).getOrigin().showPoint();
       (*it).getExtremity().showPoint();
       ++i;
   }
    
   /** APPEL A LA FONCTION PRINCIPALE **/
    std::vector<Point> inters = trouveIntersection(set);
    std::cout << "----- intersections: -----" << std::endl;
    for(unsigned j = 0; j < inters.size(); ++j)
        inters[j].showPoint();

   return 0;
}
