#include "EventQueue.h"

#include <iostream>

EventQueue::EventQueue(){

}


void EventQueue::addEvent(const Event& evt){

   std::list<Event>::iterator it;
   for(it=queue.begin(); it!=queue.end() ;++it){
   	 Event evtToCompare = (*it);
   	 if(Point::compare(evt.getPoint(), evtToCompare.getPoint()) == 0){
   	 	if(!(evt.getLSegIDs().empty()))
			evtToCompare.addLSegID(evt.getLSegIDs()[0]);
		return;
   	 }
   	 else
   	 	if(Point::compare(evt.getPoint(), evtToCompare.getPoint()) == -1)
   	 		break;
   }

   queue.insert(it, evt);
}

void EventQueue::showQueue(){

   std::list<Event>::iterator it;

   std::cout << "--------- EVENTS ---------" << std::endl;
   int i=1;
   for(it=queue.begin(); it!=queue.end() ;++it){
   	 Event toPrint = (*it);
   	 std::cout << "-- Event " << i << " :" << std::endl;
   	 toPrint.getPoint().showPoint();
     ++i;
   }

}

bool EventQueue::isEmpty(){
	return queue.empty();
}

Event EventQueue::getNextEvent(){
	Event returnVal = queue.front();
    queue.pop_front();
    return returnVal;
}

bool EventQueue::isInQueue(const Event& evt) const{
    std::list<Event>::const_iterator it;

    for(it = queue.begin() ; it != queue.end() ; ++it){
        if(Event::compare((*it), evt))
            return true;
    }

    return false;
}

