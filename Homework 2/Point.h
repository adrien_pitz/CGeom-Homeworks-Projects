#ifndef POINT_H
#define POINT_H

#include <vector>

class Point
{
public:
	Point();
	Point(long double xCoordinate, long double yCoordinate);
	void showPoint();
	long double getXCoordinate() const;
	long double getYCoordinate() const;
	void setXCoordinate(long double);
	void setYCoordinate(long double);
    static int compare(const Point& p1, const Point& p2);
    bool isEmpty() const;

private:
	long double xCoordinate;
	long double yCoordinate;
    bool isempty;
};

#endif
