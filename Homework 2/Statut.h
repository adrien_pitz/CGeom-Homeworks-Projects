#ifndef STATUT_H
#define STATUT_H

#include <map>
#include <vector>
#include <utility>
#include "Point.h"
#include "Segment.h"

/**
 * Cela permet à notre multimap d'être classée des y les plus grands au plus petit
 */
struct classcomp{
    bool operator() (const long double& op1, const long double& op2) const{
        return op1 > op2;
    }
};

class Statut{

public:
    Statut();
    Statut(const std::vector<Segment>& segmentsSet);
    //std::vector<Point> addSegment(unsigned int, long double);
    void addSegment(unsigned int, long double);
    void removeSegment(unsigned int segID);
    //Point removeSegment(unsigned int segID);
    std::multimap<long double, unsigned int, classcomp> getStatut() const;
    void clear();
    
    std::pair<long double, bool> findFromSegID(unsigned) const;
    std::pair<int, int> getNeighbours(const Point&) const;
    int upperNeighbour(unsigned sID) const;
    int lowerNeighbour(unsigned sID) const;
    void show();

private:
    std::vector<Segment> segSet;
    /**
     * Ici la clé est y, la coordonnée du point du segment LORSQUE ON L'AJOUTE DANS LE STATUT /!!\ pas nécessairement
     * le point à gauche (par exemple en cas d'intersection, pour garantir le bon ordre des segments dans le statut)
     */
    /**
     * ATTENTION, si deux éléments possèdent la même clé, ils sont triés dans le set final par ORDRE D'INSERTION
     * dans le set
     */
    std::multimap<long double, unsigned int, classcomp> statut;
};

#endif
