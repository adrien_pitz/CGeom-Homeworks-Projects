#ifndef SEGMENT_H
#define SEGMENT_H

#include "Point.h"

class Segment
{

public:
	Segment();
	Segment(const Point&, const Point&);
    Point getOrigin() const;
    Point getExtremity() const;
	int belongingTest(const Point&) const;
	long double updateY(const Point&, long double);
    
private:
    Point origin;
    Point extremity;
};

#endif
